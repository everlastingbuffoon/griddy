﻿using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class Sound {

    public AudioClip sound;

    [Range(0f,1f)]
    public float volume;
    [Range(-1f,1f)]
    public float pitch;
}