using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleMesh : MonoBehaviour {
    public Sprite shapeSprite;
    private ParticleSystem spriteParticleSystem;

    private ParticleSystemRenderer rendererSystem;
    private readonly List<Vector3> _vertices = new List<Vector3>();

    void Awake() {
        spriteParticleSystem = GetComponent<ParticleSystem>();
        var vertices = shapeSprite.vertices; // first copy of vertex buffer occurs here, when it is marshalled from unmanaged to managed memory
        var spriteMesh = new Mesh();

        _vertices.Clear();  // this temporary buffer allows me to avoid one of those allocation, but i still do copy of data because Mesh.SetVertices can't accept Vector2[]

        for (var i = 0; i < vertices.Length; i++)
        {
        _vertices.Add(vertices[i]);
        }

        spriteMesh.SetVertices(_vertices);  // here's the third copy of vertex buffer is created and marshalled back to unmanaged memory
        spriteMesh.SetTriangles(shapeSprite.triangles, 0);
        spriteMesh.SetUVs(0, shapeSprite.uv);
        spriteMesh.RecalculateBounds();

        rendererSystem = spriteParticleSystem.GetComponent<ParticleSystemRenderer>();
        rendererSystem.mesh = spriteMesh;
    }
}
