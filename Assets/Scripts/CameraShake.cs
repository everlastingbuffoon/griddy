﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour{

    public float Intensity = 0.3f;

    private Vector3 StartCameraPos;
    //float x_pos;
    //float y_pos;

    public IEnumerator ShakeDirection(float x, float y){
        StartCameraPos = transform.position;
        transform.position = new Vector3(transform.position.x+x*Intensity*0.6f, transform.position.y+y*Intensity*0.6f, transform.position.z);
        yield return new WaitForFixedUpdate();
        transform.position = new Vector3(transform.position.x-x*Intensity*0.6f*1.5f, transform.position.y-y*Intensity*0.6f*1.5f, transform.position.z);
        yield return new WaitForFixedUpdate();
        transform.position = StartCameraPos;
    }

    public IEnumerator ShakeRandom(){
        StartCameraPos = transform.position;
        float x;
        float y;
        for(int i=0; i<4; i++){
            x = Random.Range(-1,2)*Intensity;
            y = Random.Range(-1,2)*Intensity;
            transform.position = new Vector3(transform.position.x+x, transform.position.y+y, transform.position.z);
            yield return new WaitForFixedUpdate();
        }
        transform.position = StartCameraPos;
    }
}
