using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TutorialEvent {
    public GameObject Obj;
    public float x;
    public float y;
    public float z;
    public int timestamp;
    public bool activator;
    public GameObject particleObj;
}