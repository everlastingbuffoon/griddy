using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScaling : MonoBehaviour {

    CanvasScaler the_scaler;

    void Awake(){
        the_scaler = GetComponent<CanvasScaler>();
    }

    void Update(){
        if(Screen.currentResolution.width > 1920) the_scaler.matchWidthOrHeight = 1f;
        else the_scaler.matchWidthOrHeight = 0f;
    }
}
