using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScoreLight : MonoBehaviour {
    public float timer;
    public float score;
    public float multiplier;
    public float temp_multiplier;
    public GameObject scoreText_obj;
    public GameObject multiText_obj;
    public GameObject comboText_obj;
    public Mask fill_mask;
    public GameObject circle;
    // public GameObject circle_section;

    public bool awakened = false;

    private TMPro.TextMeshProUGUI scoreText;
    private TMPro.TextMeshProUGUI multiText;
    private TMPro.TextMeshProUGUI comboText;
    private Color c;

    void Awake(){
        score = 0;
        multiplier = 1;
        timer = 0;
        scoreText = scoreText_obj.GetComponent<TMPro.TextMeshProUGUI>();
        multiText = multiText_obj.GetComponent<TMPro.TextMeshProUGUI>();
        comboText = comboText_obj.GetComponent<TMPro.TextMeshProUGUI>();
        awakened = true;
    }

    void FixedUpdate(){
        /*
        if(timer >= 7.9){
            temp_multiplier = 2*multiplier;
            c = multiText.color;
            c.a = Mathf.Max(0, Mathf.Min(timer/2, 1));
            c.r = 1;
            c.b = 81f/255f;
            c.g = 208f/255f;
            multiText.color = c;
        }
        else{
        }
            */
        temp_multiplier = multiplier;
        c = multiText.color;
        c.a = Mathf.Max(0, Mathf.Min(timer/2, 1));
        c.r = 1;
        c.b = 1;
        c.g = 1;
        multiText.color = c;

        multiText.text = "\n\n\nx" + temp_multiplier;

        c = circle.GetComponent<Image>().color;
        c.a = Mathf.Max(0, Mathf.Min(timer/2, 1));
        circle.GetComponent<Image>().color = c;

        // c = circle_section.GetComponent<Image>().color;
        // c.a = Mathf.Max(0, Mathf.Min(timer/2, 1));
        // circle_section.GetComponent<Image>().color = c;

        c = comboText.color;
        c.a = Mathf.Max(0, Mathf.Min(timer/2, 1));
        comboText.color = c;

        fill_mask.GetComponent<CutoutMask>().fillAmount = timer/10f - 1f/6f;

        if (timer > 0) timer -= 0.1f + multiplier*0.003f;
        if (timer <= 0){
            multiplier = 1;
            timer = 0;
        }
    }

    public void KeepCombo(){
        if(multiplier > 1) timer = 10;
    }

    public void ScorePlus(float count){
        score += count*temp_multiplier;
        if(multiplier < 2) multiplier += 0.5f;
        else if(multiplier < 10) multiplier += 1f;
        timer = 10;

        scoreText.text = "Score: " + score;
    }
}
