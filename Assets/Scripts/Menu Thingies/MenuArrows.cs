using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuArrows : MonoBehaviour {
    public GameObject ButtonLeft;
    public GameObject ButtonRight;
    public InputMenus InputSystem;

    void OnEnable(){
        InputSystem = new InputMenus();
        InputSystem.Enable();

        InputSystem.MenuControls.MoveLeft.performed += _ => PressLeft();
        InputSystem.MenuControls.MoveRight.performed += _ => PressRight();
    }

    private void PressLeft(){
        if(ButtonLeft != null) ButtonLeft.GetComponent<Button>().onClick.Invoke();
    }

    private void PressRight(){
        if(ButtonRight != null) ButtonRight.GetComponent<Button>().onClick.Invoke();
    }

    void OnDisable(){
        InputSystem.Disable();
    }
}
