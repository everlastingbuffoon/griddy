using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotatorEnemyTutorial : MonoBehaviour {


    public bool active;
    public bool nah;
    public float rs_normal = 2f;
    public float rs_fast = 3.3f;
    public AnimationCurve rotation_management;

    public AnimationCurve color_inactive;
    public AnimationCurve color_active;
    public Image sprite;

    private float rotation_speed;
    private float time = 1f;

    void Awake(){
        sprite = GetComponent<Image>();
        nah = false;
    }

    void FixedUpdate(){
        
        Color c = sprite.color;
        if(active == false){
            rotation_speed = rs_normal;
            c.a = color_inactive.Evaluate(time);
        }
        else{
            rotation_speed = rs_fast;
            c.a = color_active.Evaluate(time);
        }
        time += Time.deltaTime;
        transform.Rotate (Vector3.forward * (rotation_management.Evaluate(time)+1) * rotation_speed);
        if(!nah) sprite.color = c;
    }
    
    public void RotationBurst(){
        time = 0f;
    }
}
