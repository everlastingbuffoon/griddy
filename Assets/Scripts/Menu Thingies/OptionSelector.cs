using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OptionSelector : MonoBehaviour {
    public InputMenus InputSystem;

    void Awake(){

    }

    void OnEnable(){
        InputSystem = new InputMenus();
        InputSystem.Enable();

        InputSystem.MenuControls.Confirm.performed += _ => PressConfirm();
        InputSystem.MenuControls.MoveLeft.performed += _ => PressLeft();
        InputSystem.MenuControls.MoveRight.performed += _ => PressRight();
    }

    void PressConfirm(){
        if(EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.GetComponent<OptionSelectorToggle>() != null){
            EventSystem.current.currentSelectedGameObject.GetComponent<OptionSelectorToggle>().InvokeToggle();
        }
        if(EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.GetComponent<OptionSelectorConfirm>() != null){
            EventSystem.current.currentSelectedGameObject.GetComponent<OptionSelectorConfirm>().InvokeButton();
        }
    }

    void PressLeft(){
        if(EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.GetComponent<OptionSelectorList>() != null){
            EventSystem.current.currentSelectedGameObject.GetComponent<OptionSelectorList>().PressLeft();
        }
    }

    void PressRight(){
        if(EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.GetComponent<OptionSelectorList>() != null){
            EventSystem.current.currentSelectedGameObject.GetComponent<OptionSelectorList>().PressRight();
        }
    }

    void OnDisable(){
        InputSystem.Disable();
    }
}
