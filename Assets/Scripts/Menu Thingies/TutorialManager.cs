using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour {
    public Canvas canvas;
    public Camera menu_camera;
    public GameObject ScoreHolder;
    public TutorialEvent[] Spawners;
    public TutorialEvent[] Activators;
    public TutorialEvent[] Movers;
    public TutorialEvent[] Attackers;
    public TutorialEvent[] Destroyers;
    public TutorialEvent[] Faders;
    public TutorialEvent[] ColorChanges;
    public TutorialEvent[] ButtonPresses;
    public TutorialEvent[] Shakes;
    public TutorialEvent[] ScoreReseters;
    public TutorialEvent[] ScoreActivators;
    public int max_time = 1000;

    private int timer;
    private Dictionary<GameObject, Coroutine> MovementTable = new Dictionary<GameObject, Coroutine>();

    void Awake(){
        ResetTimer();
    }

    void OnEnable(){
        ResetTimer();
    }

    void ResetTimer(){
        timer = 0;
    }

    void FixedUpdate(){
        timer += 1;
        timer %= max_time;

        for(int i=0; i<Spawners.Length; i++)
            if(Spawners[i].timestamp == timer)
                SpawnObject(Spawners[i].Obj, Spawners[i].x, Spawners[i].y, Spawners[i].activator);

        for(int i=0; i<Activators.Length; i++)
            if(Activators[i].timestamp == timer)
                ActiveObject(Activators[i].Obj, Activators[i].activator);

        for(int i=0; i<Movers.Length; i++)
            if(Movers[i].timestamp == timer)
                MoveObject(Movers[i].Obj, Movers[i].x, Movers[i].y);

        for(int i=0; i<Attackers.Length; i++)
            if(Attackers[i].timestamp == timer)
                AttackObject(Attackers[i].Obj);

        for(int i=0; i<Destroyers.Length; i++)
            if(Destroyers[i].timestamp == timer)
                DestroyObject(Destroyers[i].Obj, null);

        for(int i=0; i<Faders.Length; i++)
            if(Faders[i].timestamp == timer)
                FadeObject(Faders[i].Obj);

        for(int i=0; i<ColorChanges.Length; i++)
            if(ColorChanges[i].timestamp == timer)
                ColorObject(ColorChanges[i].Obj, ColorChanges[i].x, ColorChanges[i].y, ColorChanges[i].z, 255);

        for(int i=0; i<ButtonPresses.Length; i++)
            if(ButtonPresses[i].timestamp == timer)
                PressButton(ButtonPresses[i].Obj);

        for(int i=0; i<Shakes.Length; i++)
            if(Shakes[i].timestamp == timer)
                ShakeArea(Shakes[i].Obj, 10*Shakes[i].x, 10*Shakes[i].y);

        for(int i=0; i<ScoreReseters.Length; i++)
            if(ScoreReseters[i].timestamp == timer)
                ResetScore();

        for(int i=0; i<ScoreActivators.Length; i++)
            if(ScoreActivators[i].timestamp == timer)
                ScorePoint();
    }

    // ~ //

    public void SpawnObject(GameObject obj, float x, float y, bool flash){
        if(MovementTable.ContainsKey(obj)){
            if(MovementTable[obj] != null) StopCoroutine(MovementTable[obj]);
            MovementTable.Remove(obj);
        }

        obj.GetComponent<RectTransform>().localPosition = new Vector2(x, y);
        obj.SetActive(true);
        if(flash){
            StartCoroutine(ImageFlash(obj));
        }
    }

    public void ActiveObject(GameObject obj, bool active){
        obj.GetComponent<RotatorEnemyTutorial>().nah = false;
        obj.GetComponent<RotatorEnemyTutorial>().active = active;
    }

    public void MoveObject(GameObject obj, float x, float y){
        if(obj.GetComponent<RotationPickup>() != null) obj.GetComponent<RotationPickup>().RotationBurst();
        else if(obj.GetComponent<RotatorEnemyTutorial>() != null) obj.GetComponent<RotatorEnemyTutorial>().RotationBurst();

        if(MovementTable.ContainsKey(obj)){
            if(MovementTable[obj] != null) StopCoroutine(MovementTable[obj]);
            MovementTable.Remove(obj);
        }
        MovementTable.Add(obj, StartCoroutine(SmoothMovement(obj, obj.GetComponent<RectTransform>().localPosition, new Vector2(x, y))));
    }

    public void AttackObject(GameObject obj){
        if(obj.GetComponent<RotationPickup>() != null) obj.GetComponent<RotationPickup>().RotationBurst();
        else if(obj.GetComponent<RotatorEnemyTutorial>() != null) obj.GetComponent<RotatorEnemyTutorial>().RotationBurst();
    }

    public void DestroyObject(GameObject obj, GameObject particles){
        if(MovementTable.ContainsKey(obj)){
            if(MovementTable[obj] != null) StopCoroutine(MovementTable[obj]);
            MovementTable.Remove(obj);
        }

        obj.SetActive(false);
        if(particles != null){
            // Particles here
        }
    }

    public void FadeObject(GameObject obj){
        StartCoroutine(ImageFade(obj));
        if(obj.GetComponent<RotatorEnemyTutorial>() != null) obj.GetComponent<RotatorEnemyTutorial>().nah = true;
    }

    public void ColorObject(GameObject obj, float r, float g, float b, float a){
        Color new_color = new Color(r/255, g/255, b/255, a/255);
        obj.GetComponent<Image>().color = new_color;
        // if(obj.GetComponent<RotatorEnemyTutorial>() != null) obj.GetComponent<RotatorEnemyTutorial>().nah = true;
    }

    public void PressButton(GameObject obj){
        StartCoroutine(Press(obj));
    }

    public void ShakeArea(GameObject obj, float x, float y){
        StartCoroutine(ShakeDirection(obj, x, y));
    }

    public void ResetScore(){
        ScoreHolder.GetComponent<ScoreLight>().score = 0;
        ScoreHolder.GetComponent<ScoreLight>().scoreText_obj.GetComponent<TMPro.TextMeshProUGUI>().text = "Score: " + ScoreHolder.GetComponent<ScoreLight>().score;
        ScoreHolder.GetComponent<ScoreLight>().timer = 0;
    }

    public void ScorePoint(){
        ScoreHolder.GetComponent<ScoreLight>().ScorePlus(10);
    }

    // ~ //

    protected IEnumerator SmoothMovement(GameObject obj, Vector2 start, Vector2 Marker){
        float sqrRemainingDistance = (new Vector2(obj.GetComponent<RectTransform>().localPosition.x, obj.GetComponent<RectTransform>().localPosition.y) - Marker).sqrMagnitude;
        while(sqrRemainingDistance > float.Epsilon){ //float.Epsilon
            obj.GetComponent<RectTransform>().localPosition = Vector2.Lerp(obj.GetComponent<RectTransform>().localPosition, new Vector2(Marker.x, Marker.y), 15 * Time.deltaTime);
            sqrRemainingDistance = (new Vector2(obj.GetComponent<RectTransform>().localPosition.x, obj.GetComponent<RectTransform>().localPosition.y) - Marker).sqrMagnitude;
            yield return new WaitForFixedUpdate();
        }
    }

    public IEnumerator ImageFlash(GameObject obj){
        Image image = obj.GetComponent<Image>();
        int divisions = 20;
    
        Color starting_color = image.color;
        Color new_color = new Color(1, 1, 1, 1);
        float color_dif_r = (1-starting_color.r)/divisions;
        float color_dif_g = (1-starting_color.g)/divisions;
        float color_dif_b = (1-starting_color.b)/divisions;
        float color_dif_a = (1-starting_color.a)/divisions;

        for(int i=0; i<divisions; i++){
            if(obj == null) yield break;
            image.color = new_color;
            new_color = new Color(new_color.r - color_dif_r, new_color.g - color_dif_g, new_color.b - color_dif_b, new_color.a - color_dif_a);
            yield return new WaitForFixedUpdate();
        }

        if(obj != null) image.color = starting_color;
    }

    public IEnumerator ImageFade(GameObject obj){
        Image image = obj.GetComponent<Image>();
        int divisions = 35;
    
        Color starting_color = image.color;
        Color new_color = new Color(starting_color.r, starting_color.g, starting_color.b, 1);

        for(int i=0; i<divisions; i++){
            if(obj == null) yield break;
            image.color = new_color;
            new_color = new Color(starting_color.r, starting_color.g, starting_color.b, 1.0f - (float)i/(float)divisions);
            yield return new WaitForFixedUpdate();
        }

        if(obj != null) image.color = new Color(starting_color.r, starting_color.g, starting_color.b, 0f);
    }

    public IEnumerator ShakeDirection(GameObject obj, float x, float y){
        Vector3 StartingPos = obj.GetComponent<RectTransform>().localPosition;
        obj.GetComponent<RectTransform>().localPosition = new Vector3(obj.GetComponent<RectTransform>().localPosition.x+x*0.6f, obj.GetComponent<RectTransform>().localPosition.y+y*0.6f, obj.GetComponent<RectTransform>().localPosition.z);
        yield return new WaitForFixedUpdate();
        obj.GetComponent<RectTransform>().localPosition = new Vector3(obj.GetComponent<RectTransform>().localPosition.x-x*0.6f*1.5f, obj.GetComponent<RectTransform>().localPosition.y-y*0.6f*1.5f, obj.GetComponent<RectTransform>().localPosition.z);
        yield return new WaitForFixedUpdate();
        obj.GetComponent<RectTransform>().localPosition = StartingPos;
    }

    public IEnumerator Press(GameObject obj){
        Vector3 StartingPos = obj.GetComponent<RectTransform>().localPosition;
        obj.GetComponent<RectTransform>().localPosition = new Vector3(obj.GetComponent<RectTransform>().localPosition.x, obj.GetComponent<RectTransform>().localPosition.y-1.5f, obj.GetComponent<RectTransform>().localPosition.z);
        obj.GetComponent<Image>().color = new Color(obj.GetComponent<Image>().color.r, obj.GetComponent<Image>().color.g, obj.GetComponent<Image>().color.b, 255f/255f);
        yield return new WaitForSeconds(0.13f);
        obj.GetComponent<RectTransform>().localPosition = StartingPos;
        obj.GetComponent<Image>().color = new Color(obj.GetComponent<Image>().color.r, obj.GetComponent<Image>().color.g, obj.GetComponent<Image>().color.b, 200f/255f);
    }
}
