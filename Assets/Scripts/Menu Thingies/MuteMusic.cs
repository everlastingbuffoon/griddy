using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteMusic : MonoBehaviour {
    private Toggle ToggleButton;

    void Awake(){
        ToggleButton = GetComponent<Toggle>();
        StartCoroutine(MuteThatMusic());

        ToggleButton.onValueChanged.AddListener(delegate {
            MusicManager.instance.Mute(ToggleButton.isOn);
        });
    }

    IEnumerator MuteThatMusic(){
        yield return new WaitUntil(() => MusicManager.instance != null);
        MusicManager.instance.Mute(ToggleButton.isOn);
    }
}
