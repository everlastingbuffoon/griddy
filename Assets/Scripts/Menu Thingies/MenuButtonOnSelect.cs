using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuButtonOnSelect : MonoBehaviour, ISelectHandler{
    public GameObject main_menu;

    public void OnSelect (BaseEventData eventData){
        //main_menu.GetComponent<MainMenu>().RemovePointer();
        if(main_menu.GetComponent<MainMenu>().pointer == null) main_menu.GetComponent<MainMenu>().SpawnPointer(transform.position.x - (GetComponent<RectTransform>().rect.width/2), transform.position.y);
        else main_menu.GetComponent<MainMenu>().MovePointer(transform.position.x - (GetComponent<RectTransform>().rect.width/2), transform.position.y);
    }
}
