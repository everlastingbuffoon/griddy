using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionList : MonoBehaviour {
    public GameObject OptionsMenu;

    public List<string> ListOfThings;
    public int cur_value;
    public GameObject ButtonPrev;
    public GameObject ButtonNext;

    public GameObject ListText_obj;
    private TMPro.TextMeshProUGUI ListText;

    void Awake(){
        ListText = ListText_obj.GetComponent<TMPro.TextMeshProUGUI>();
    }

    void Update(){
        if(cur_value == 0){
            ButtonPrev.SetActive(false);
            ButtonNext.SetActive(true);
        }
        else if(cur_value == ListOfThings.Count-1){
            ButtonPrev.SetActive(true);
            ButtonNext.SetActive(false);
        }
        else {
            ButtonPrev.SetActive(true);
            ButtonNext.SetActive(true);
        }

        ListText.text = ListOfThings[cur_value];
    }

    public void NextElement(){
        if(cur_value < ListOfThings.Count-1) cur_value += 1;
        if(OptionsMenu != null) OptionsMenu.GetComponent<OptionsMenu>().UpdateTheScreen();
    }

    public void PrevElement(){
        if(cur_value > 0) cur_value -= 1;
        if(OptionsMenu != null) OptionsMenu.GetComponent<OptionsMenu>().UpdateTheScreen();
    }
}
