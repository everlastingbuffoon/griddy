using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MouseManager : MonoBehaviour {
    public InputMenus InputSystem;
    public GraphicRaycaster CanvasRaycaster;
    public bool usingMouse;
    [HideInInspector] public bool CanvasRaycasterLock;

    static public MouseManager instance;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        CanvasRaycasterLock = false;
    }

    void OnEnable(){
        InputSystem = new InputMenus();
        InputSystem.Enable();

        InputSystem.MenuControls.AnyKey.performed += _ => HideMouse();
        InputSystem.MenuControls.AnyMouseInput.performed += _ => ShowMouse();
        InputSystem.MenuControls.Point.performed += _ => ShowMouse();
    }

    public void ShowMouse(){
        if(CanvasRaycaster != null && CanvasRaycasterLock == false) CanvasRaycaster.enabled = true;
        //Cursor.visible = true;
        //Cursor.lockState = CursorLockMode.None;
        usingMouse = true;
    }

    public void HideMouse(){
        if(CanvasRaycaster != null && CanvasRaycasterLock == false) CanvasRaycaster.enabled = false;
        //Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Locked;
        usingMouse = false;
    }

    public void LockCanvasRaycaster(bool set){
        CanvasRaycasterLock = set;
    }

    void OnDisable(){
        InputSystem.Disable();
    }
}
