using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GeneralMenu : MonoBehaviour {
    public GameObject keyboard_first_button;

    public GameObject pointer_holder;
    public GameObject pointer_obj;
    public GameObject pointer;

    private Coroutine last_coroutine = null;

    public void GameStart(){
        SceneManager.LoadScene("Game");
    }

    public void BackToMenu(){
        //ShowMouse();
        SceneManager.LoadScene("Menu");
    }

    public void Quit(){
        Application.Quit();
    }

    public void SpawnPointer(float x, float y){
        if(pointer == null){
            pointer = Instantiate(pointer_obj, new Vector3 (x, y, pointer_obj.transform.position.z), Quaternion.identity);
            pointer.transform.SetParent(pointer_holder.transform);
        }
    }

    public void MovePointer(float x, float y){
        if(pointer == null) return;
        if(last_coroutine != null) StopCoroutine(last_coroutine);
        last_coroutine = StartCoroutine(SmoothMovement(new Vector3(pointer.transform.position.x, pointer.transform.position.y, pointer.transform.position.z), new Vector3(x, y, pointer.transform.position.z)));
    }

    protected IEnumerator SmoothMovement(Vector3 start, Vector3 Marker){
        Rigidbody2D rb2d = pointer.GetComponent<Rigidbody2D>();

        float sqrRemainingDistance = (pointer.transform.position - Marker).sqrMagnitude;
        while(sqrRemainingDistance > float.Epsilon){
            Vector3 newPosition = Vector3.MoveTowards(rb2d.position, Marker, 600 * Time.deltaTime);
            rb2d.MovePosition(newPosition);
            sqrRemainingDistance = (pointer.transform.position - Marker).sqrMagnitude;
            yield return new WaitForFixedUpdate();
        }
        last_coroutine = null;
    }

    public void RemovePointer(){
        if(pointer != null) Destroy(pointer);
        pointer = null;
    }
}
