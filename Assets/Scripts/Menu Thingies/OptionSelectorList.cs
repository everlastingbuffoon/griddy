using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionSelectorList : MonoBehaviour {
    public Button ButtonLeft;
    public Button ButtonRight;

    public void PressLeft(){
        if(ButtonLeft.isActiveAndEnabled) ButtonLeft.onClick.Invoke();
    }

    public void PressRight(){
        if(ButtonRight.isActiveAndEnabled) ButtonRight.onClick.Invoke();
    }
}
