using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteSound : MonoBehaviour {
    private Toggle ToggleButton;

    void Awake(){
        ToggleButton = GetComponent<Toggle>();
        StartCoroutine(MuteThatSound());

        ToggleButton.onValueChanged.AddListener(delegate {
            SoundManager.instance.Mute(ToggleButton.isOn);
        });
    }

    IEnumerator MuteThatSound(){
        yield return new WaitUntil(() => SoundManager.instance != null);
        SoundManager.instance.Mute(ToggleButton.isOn);
    }
}
