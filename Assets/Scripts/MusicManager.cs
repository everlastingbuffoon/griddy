﻿using UnityEngine.Audio;
using UnityEngine;

public class MusicManager : MonoBehaviour{

    public static MusicManager instance = null;
    public Sound[] misc_sounds;
    public AudioSource music_audioSrc;

    private bool muted;
    private AudioClip current_music;
    public AudioClip last_music;

    void Awake(){
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        music_audioSrc.loop = true;
    }

    public void PlayMusic(AudioClip music){
        last_music = music;
        if(music != current_music){
            current_music = music;
            if(!muted){
                music_audioSrc.Stop();
                music_audioSrc.clip = music;
                music_audioSrc.Play();
            }
        }
    }

    public void StopMusic(){
        current_music = null;
        music_audioSrc.Stop();
    }

    public void Mute(bool check){
        muted = check;
        if(muted) StopMusic();
        else PlayMusic(last_music);
    }
}