﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationPlayer : MonoBehaviour {
    public float rotation_speed = -0.8f;
    public AnimationCurve rotation_management;

    private float time = 1f;

    void FixedUpdate(){
        time += Time.deltaTime;
        transform.Rotate (Vector3.forward * (rotation_management.Evaluate(time)+1) * rotation_speed);
    }
    
    public void RotationBurst(){
        time = 0f;
    }
}
