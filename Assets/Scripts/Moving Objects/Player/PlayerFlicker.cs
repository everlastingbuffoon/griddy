﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFlicker : MonoBehaviour {

    public AnimationCurve flicker_management;
    public Player player;
    public SpriteRenderer sprite;

    private float time = 0f;
    private Color alphaFlicker;

    void Awake(){
        player = GetComponent<Player>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void FixedUpdate(){
        time += Time.deltaTime;

        alphaFlicker = sprite.color;
        //if(player.health <= 0) alphaFlicker.a = 0;
        //else alphaFlicker.a = flicker_management.Evaluate(time);
        sprite.color = alphaFlicker;
    }
}