using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomba : Destructable {
    public Position position;
    public GameObject pinksplosion;
    
    private int max_explosion;

    public override void Die(int xDir, int yDir){
        GameManager.instance.SecretGrid[position.x, position.y].unit = null;

        GameManager.instance.scoreManager.KeepCombo();
        if(PlayerManager.instance.player_wait > 0) PlayerManager.instance.player_wait -= 1;

        // particles
        if(DeathParticles != null){
            foreach(ParticleSystem ParticleSet in DeathParticles.GetComponentsInChildren<ParticleSystem>()){
                var shape = ParticleSet.shape;
                shape.rotation = new Vector3(0, 0, (2*((xDir==-1) ? 1 : 0)+yDir)*90-(ParticleSet.shape.arc/2));
            }
        }

        GameObject particels = Instantiate(DeathParticles, transform.position, Quaternion.identity);
        particels.transform.SetParent(GameManager.instance.Particles.transform);
        
        // T.D.P: Tactical Diagonal Pinksplosion
        max_explosion = 0;

        int i=1;
        while(true){
            if(position.x+i >= GameManager.instance.SecretGrid.GetLength(0) || position.y+i >= GameManager.instance.SecretGrid.GetLength(1)) break;
            if(GameManager.instance.SecretGrid[position.x+i, position.y+i].tile == null) break;
            KillSpot(position.x+i, position.y+i);
            //Instantiate(pinksplosion, new Vector3(position.x+i, position.y+i, 10), Quaternion.identity);    // TEMP
            if(i > max_explosion) max_explosion = i;
            i++;
        }
        
        i=1;
        while(true){
            if(position.x+i >= GameManager.instance.SecretGrid.GetLength(0) || position.y-i < 0) break;
            if(GameManager.instance.SecretGrid[position.x+i, position.y-i].tile == null) break;
            KillSpot(position.x+i, position.y-i);
            //Instantiate(pinksplosion, new Vector3(position.x+i, position.y-i, 10), Quaternion.identity);    // TEMP
            if(i > max_explosion) max_explosion = i;
            i++;
        }

        i=1;
        while(true){
            if(position.x-i < 0 || position.y+i >= GameManager.instance.SecretGrid.GetLength(1)) break;
            if(GameManager.instance.SecretGrid[position.x-i, position.y+i].tile == null) break;
            KillSpot(position.x-i, position.y+i);
            //Instantiate(pinksplosion, new Vector3(position.x-i, position.y+i, 10), Quaternion.identity);    // TEMP
            if(i > max_explosion) max_explosion = i;
            i++;
        }

        i=1;
        while(true){
            if(position.x-i < 0 || position.y-i < 0) break;
            if(GameManager.instance.SecretGrid[position.x-i, position.y-i].tile == null) break;
            KillSpot(position.x-i, position.y-i);
            //Instantiate(pinksplosion, new Vector3(position.x-i, position.y-i, 10), Quaternion.identity);    // TEMP
            if(i > max_explosion) max_explosion = i;
            i++;
        }

        GameObject pink = Instantiate(pinksplosion, new Vector3(position.x, position.y, 10), Quaternion.identity);
        DontDestroyOnLoad(pink);
        pink.GetComponent<Pinksplosion>().base_x = position.x;
        pink.GetComponent<Pinksplosion>().base_y = position.y;
        pink.GetComponent<Pinksplosion>().step_count = max_explosion+1;

        gameObject.SetActive(false);
        Destroy(gameObject);
    }

    public void KillSpot(int x, int y){
        GameObject hit = GameManager.instance.SecretGrid[x, y].unit;
        if(hit == null){ return; }
        else if(hit.CompareTag("Player")){ StartCoroutine(PlayerExplode(hit)); }
        else if(hit.CompareTag("Enemy")){ hit.GetComponent<Enemy>().to_die = true; hit.GetComponent<MovingObject>().Die(0, 0); }
        else if(hit.CompareTag("Destructable")){ hit.GetComponent<Destructable>().Die(0, 0); }
    }

    private IEnumerator PlayerExplode(GameObject player){
        yield return new WaitUntil(() => PlayerManager.instance.attack_phase == false);
        PlayerManager.instance.RemovePlayer(player, this.transform.parent.gameObject);
        player.GetComponent<MovingObject>().Die(0, 0);
    }
}
