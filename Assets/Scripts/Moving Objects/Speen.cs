using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speen : MonoBehaviour {
    public float rotation_speed = 5f;

    private float time = 1f;

    void FixedUpdate(){
        time += Time.deltaTime;
        transform.Rotate (Vector3.forward * rotation_speed);
        transform.Rotate (Vector3.up * rotation_speed);
        transform.Rotate (Vector3.right * rotation_speed);
    }
}
