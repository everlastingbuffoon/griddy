﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexagonHealth : MonoBehaviour {

    public int health_num = 0;
    public Enemy enemy;
    public SpriteRenderer sprite;

    private Color alpha;

    void Awake(){
        enemy = transform.parent.gameObject.GetComponent<Enemy>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void Update(){
        alpha = sprite.color;
        //if(enemy.health < health_num) alpha.a = 0;
        //else alpha.a = 1;
        sprite.color = alpha;
    }
}