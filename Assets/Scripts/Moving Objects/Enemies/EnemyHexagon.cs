﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHexagon : Enemy {
    private ColorEnemy color;

    private bool invulnerable;
    private bool to_teleport;
    private bool keep_turn;
    private Position teleport_target;

    public GameObject[] pieces;
    public Color base_color;
    public Color tele_color;
    public AudioClip teleport_sound;

    protected override void Awake(){
        base.Awake();
        rotator = GetComponent<RotationEnemies>();
        invulnerable = true;
        to_teleport = false;
        keep_turn = false;
    }

    public override bool TurnCounter(){
        if(takeTurn == false){
            takeTurn = true;
            return false;
        }
        else{
            if(keep_turn) keep_turn = false;
            else takeTurn = false;
            return true;
        }
    }

    public override int EnemyTurn(){
        if(to_teleport == true){
            to_teleport = false;
            invulnerable = false;
            keep_turn = true;
            priority = 2;
            SoundManager.instance.PlaySound(teleport_sound);

            Position teleport_pos = new Position(position.x, position.y);
            if(GameManager.instance.SecretGrid[teleport_pos.x, teleport_pos.y].unit == null) return Teleport(teleport_pos.x, teleport_pos.y);
            else if(GameManager.instance.SecretGrid[teleport_pos.x, teleport_pos.y].unit.GetComponent<MovingObject>() == null) return -1;
            else{
                Position next_pos = GameManager.instance.SecretGrid[teleport_pos.x, teleport_pos.y].unit.GetComponent<MovingObject>().previous_position;
                Position direction_pos = new Position(next_pos.x - position.x, next_pos.y - position.y);
                while(GameManager.instance.SecretGrid[teleport_pos.x, teleport_pos.y].unit != null){
                    if(GameManager.instance.SecretGrid[teleport_pos.x, teleport_pos.y].unit.tag == "Void"){
                        transform.position = new Vector2(teleport_pos.x, teleport_pos.y);
                        return -1;
                    }
                    else if(GameManager.instance.SecretGrid[teleport_pos.x, teleport_pos.y].unit.tag == "Player" && GameManager.instance.SecretGrid[teleport_pos.x, teleport_pos.y].unit.GetComponent<Player>().invulnerable){
                        transform.position = new Vector2(teleport_pos.x, teleport_pos.y);
                        SoundManager.instance.PlaySound(PlayerManager.instance.attack_sound);
                        return -1;
                    }
                    
                    teleport_pos = new Position(teleport_pos.x+direction_pos.x, teleport_pos.y+direction_pos.y);
                }
            }

            /*
            while(GameManager.instance.SecretGrid[teleport_pos.x, teleport_pos.y].unit != null){
                if(GameManager.instance.SecretGrid[teleport_pos.x, teleport_pos.y].unit.GetComponent<MovingObject>() == null) return 0;
                Position next_pos = GameManager.instance.SecretGrid[teleport_pos.x, teleport_pos.y].unit.GetComponent<MovingObject>().previous_position;
                if(next_pos == teleport_pos) return 0;
                teleport_pos = next_pos;
            }
            */

            return Teleport(teleport_pos.x, teleport_pos.y);
        }

        invulnerable = true;
        TeleModeOff();
        (int, int) the_move = MoveEnemy();
        if (the_move == (0,0)) return 0;
        to_teleport = false;
        return Move(the_move.Item1, the_move.Item2);
    }

    public override (int, int) MoveEnemy(){
        List<(Position, Position)> toExplore = new List<(Position, Position)>();
        List<(int, int)> Already = new List<(int, int)>();
        Already.Add((position.x, position.y));

        // appetizers
        if(GameManager.instance.SecretGrid[position.x+1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y].unit.tag == "Player" && !GameManager.instance.SecretGrid[position.x+1, position.y].unit.GetComponent<Player>().invulnerable){
                return (1, 0);
            }
        }
        else{
            toExplore.Add((new Position(position.x+1, position.y), new Position(1, 0)));
        }
        
        if(GameManager.instance.SecretGrid[position.x-1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y].unit.tag == "Player" && !GameManager.instance.SecretGrid[position.x-1, position.y].unit.GetComponent<Player>().invulnerable){
                return (-1, 0);
            }
        }
        else{
            toExplore.Add((new Position(position.x-1, position.y), new Position(-1, 0)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y+1].unit.tag == "Player" && !GameManager.instance.SecretGrid[position.x, position.y+1].unit.GetComponent<Player>().invulnerable){
                return (0, 1);
            }
        }
        else{
            toExplore.Add((new Position(position.x, position.y+1), new Position(0, 1)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y-1].unit.tag == "Player" && !GameManager.instance.SecretGrid[position.x, position.y-1].unit.GetComponent<Player>().invulnerable){
                return (0, -1);
            }
        }
        else{
            toExplore.Add((new Position(position.x, position.y-1), new Position(0, -1)));
        }

        /*
        if(GameManager.instance.SecretGrid[position.x+1, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y+1].unit.tag == "Player"){
                return (1, 1);
            }
        }
        else{
            toExplore.Add((new Position(position.x+1, position.y+1), new Position(1, 1)));
        }
        
        if(GameManager.instance.SecretGrid[position.x-1, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y+1].unit.tag == "Player"){
                return (-1, 1);
            }
        }
        else{
            toExplore.Add((new Position(position.x-1, position.y+1), new Position(-1, 1)));
        }

        if(GameManager.instance.SecretGrid[position.x+1, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y-1].unit.tag == "Player"){
                return (1, -1);
            }
        }
        else{
            toExplore.Add((new Position(position.x+1, position.y-1), new Position(1, -1)));
        }

        if(GameManager.instance.SecretGrid[position.x-1, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y-1].unit.tag == "Player"){
                return (-1, -1);
            }
        }
        else{
            toExplore.Add((new Position(position.x-1, position.y-1), new Position(-1, -1)));
        }
        */

        // all the good stuff
        while(toExplore.Count > 0){
            // organise
            mergin(toExplore);
            foreach((Position, Position) P in toExplore){
                if(!Already.Contains((P.Item1.x, P.Item1.y))){
                    Already.Add((P.Item1.x, P.Item1.y));
                }
            }
            //Already.Add((toExplore[0].Item1.x, toExplore[0].Item1.y));
/*
            // trap check
            for(int i=0; i<GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc.Count; i++){
                if(!Already.Contains((toExplore[0].Item1.x+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_x, toExplore[0].Item1.y+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_x, toExplore[0].Item1.y+GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc[i].GetComponent<ItsATrap>().push_y), toExplore[0].Item2));
                }
            }
            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y].misc.Count>0){
                toExplore.RemoveAt(0);
                continue;
            }
*/
            // do the boogaloo
            /*
            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y+1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y+1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y+1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y+1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y+1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y+1))){
                toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y+1), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y+1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y+1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y+1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y+1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y+1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y+1))){
                toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y+1), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y-1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y-1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y-1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y-1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y-1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y-1))){
                toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y-1), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y-1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y-1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y-1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y-1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y-1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y-1))){
                toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y-1), toExplore[0].Item2));
            }
            */

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x+1, toExplore[0].Item1.y].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x+1, toExplore[0].Item1.y))){
                toExplore.Add((new Position(toExplore[0].Item1.x+1, toExplore[0].Item1.y), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x-1, toExplore[0].Item1.y].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y))){
                    toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x-1, toExplore[0].Item1.y))){
                toExplore.Add((new Position(toExplore[0].Item1.x-1, toExplore[0].Item1.y), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y+1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y+1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y+1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y+1))){
                toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y+1), toExplore[0].Item2));
            }

            if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit != null){
                if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit.tag == "Player"){
                    return (toExplore[0].Item2.x, toExplore[0].Item2.y);
                }
                else if(GameManager.instance.SecretGrid[toExplore[0].Item1.x, toExplore[0].Item1.y-1].unit.CompareTag("Enemy") && !Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y-1))){
                    toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y-1), toExplore[0].Item2));
                }
            }
            else if(!Already.Contains((toExplore[0].Item1.x, toExplore[0].Item1.y-1))){
                toExplore.Add((new Position(toExplore[0].Item1.x, toExplore[0].Item1.y-1), toExplore[0].Item2));
            }

            toExplore.RemoveAt(0);
        }

        // failed search
        return (0, 0);
    }

    public override (int, int) PanicMove(){
        TeleModeOff();
        List<(Position, Position)> PossibleMoves = new List<(Position, Position)>();

        // find possible moves
        if(GameManager.instance.SecretGrid[position.x+1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y].unit.tag == "Player"){
                return (1, 0);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x+1, position.y), new Position(1, 0)));
        }
        
        if(GameManager.instance.SecretGrid[position.x-1, position.y].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y].unit.tag == "Player"){
                return (-1, 0);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x-1, position.y), new Position(-1, 0)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y+1].unit.tag == "Player"){
                return (0, 1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x, position.y+1), new Position(0, 1)));
        }

        if(GameManager.instance.SecretGrid[position.x, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x, position.y-1].unit.tag == "Player"){
                return (0, -1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x, position.y-1), new Position(0, -1)));
        }

        /*
        if(GameManager.instance.SecretGrid[position.x+1, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y+1].unit.tag == "Player"){
                return (1, 1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x+1, position.y+1), new Position(1, 1)));
        }
        
        if(GameManager.instance.SecretGrid[position.x-1, position.y+1].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y+1].unit.tag == "Player"){
                return (-1, 1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x-1, position.y+1), new Position(-1, 1)));
        }

        if(GameManager.instance.SecretGrid[position.x+1, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x+1, position.y-1].unit.tag == "Player"){
                return (1, -1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x+1, position.y-1), new Position(1, -1)));
        }

        if(GameManager.instance.SecretGrid[position.x-1, position.y-1].unit != null){
            if(GameManager.instance.SecretGrid[position.x-1, position.y-1].unit.tag == "Player"){
                return (-1, -1);
            }
        }
        else{
            PossibleMoves.Add((new Position(position.x-1, position.y-1), new Position(-1, -1)));
        }
        */

        // put moves in order
        if(PossibleMoves.Count > 0){
            // organise
            mergin(PossibleMoves);
            return (PossibleMoves[0].Item2.x, PossibleMoves[0].Item2.y);
        }

        // failed search
        return (0, 0);
    }

    public override int ReturnExistance(){
        if(!invulnerable) return 2;

        takeTurn = true;
        GameManager.instance.SecretGrid[position.x, position.y].unit = null;
        to_teleport = true;
        priority = 0;
        TeleModeOn();
        return 1;
    }

    private void TeleModeOn(){
        pieces[0].transform.localPosition = new Vector2(0, 0);
        pieces[0].GetComponent<ColorEnemy>().base_color = tele_color;
        pieces[0].GetComponent<ColorEnemy>().time = 0;

        pieces[1].transform.localPosition = new Vector2(0, 0);
        pieces[1].GetComponent<ColorEnemy>().base_color = tele_color;
        pieces[1].GetComponent<ColorEnemy>().time = 0;

        pieces[2].transform.localPosition = new Vector2(0, 0);
        pieces[2].GetComponent<ColorEnemy>().base_color = tele_color;
        pieces[2].GetComponent<ColorEnemy>().time = 0;

        pieces[3].transform.localPosition = new Vector2(0, 0);
        pieces[3].GetComponent<ColorEnemy>().base_color = tele_color;
        pieces[3].GetComponent<ColorEnemy>().time = 0;
    }

    private void TeleModeOff(){
        pieces[0].transform.localPosition = new Vector2(-0.0333f, 0.0333f);
        pieces[0].GetComponent<ColorEnemy>().base_color = base_color;
        pieces[0].GetComponent<ColorEnemy>().time = pieces[0].GetComponent<ColorEnemy>().time_offset;

        pieces[1].transform.localPosition = new Vector2(-0.0333f, -0.0333f);
        pieces[1].GetComponent<ColorEnemy>().base_color = base_color;
        pieces[1].GetComponent<ColorEnemy>().time = pieces[1].GetComponent<ColorEnemy>().time_offset;

        pieces[2].transform.localPosition = new Vector2(0.0333f, -0.0333f);
        pieces[2].GetComponent<ColorEnemy>().base_color = base_color;
        pieces[2].GetComponent<ColorEnemy>().time = pieces[2].GetComponent<ColorEnemy>().time_offset;
        
        pieces[3].transform.localPosition = new Vector2(0.0333f, 0.0333f);
        pieces[3].GetComponent<ColorEnemy>().base_color = base_color;
        pieces[3].GetComponent<ColorEnemy>().time = pieces[3].GetComponent<ColorEnemy>().time_offset;
    }
}