﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationEnemies : MonoBehaviour {
    protected float rotation_speed;

    public float rs_normal = 2f;
    public float rs_fast = 3.3f;
    public AnimationCurve rotation_management;
    public Enemy enemy;

    [HideInInspector] public float time = 1f;

    protected virtual void Awake(){
        enemy = GetComponent<Enemy>();
    }

    protected virtual void FixedUpdate(){
        if(enemy.takeTurn == false){
            rotation_speed = rs_normal;
        }
        else rotation_speed = rs_fast;
        time += Time.deltaTime;
        transform.Rotate (Vector3.forward * (rotation_management.Evaluate(time)+1) * rotation_speed);
    }
    
    public void RotationBurst(){
        time = 0f;
    }

    public void RotationBurstSmall(){
        time = 0.4f;
    }
}