﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationEnemyHex : RotationEnemies {
    protected override void Awake(){
        base.Awake();
    }

    protected override void FixedUpdate(){
        if(enemy.takeTurn == false){
            rotation_speed = rs_normal;
        }
        else rotation_speed = rs_fast;
        time += Time.deltaTime;
        transform.Rotate (Vector3.forward * (rotation_management.Evaluate(time)+1) * rotation_speed);
    }
}