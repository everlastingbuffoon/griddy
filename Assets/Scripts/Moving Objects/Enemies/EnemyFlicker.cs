﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlicker : MonoBehaviour {
    public AnimationCurve flicker_management;
    public Enemy enemy;
    public SpriteRenderer sprite;

    private float time = 0f;
    private Color alphaFlicker;

    void Awake(){
        enemy = GetComponent<Enemy>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void FixedUpdate(){
        time += Time.deltaTime;

        alphaFlicker = sprite.color;
        //if(enemy.health <= 0) alphaFlicker.a = 0;
        //else alphaFlicker.a = flicker_management.Evaluate(time);
        sprite.color = alphaFlicker;
    }
}