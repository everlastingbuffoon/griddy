using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Boss : Enemy {
    public int boss_level;
    public AudioClip spawn_sound;

    protected IEnumerator WaitAndDieAndSpawn(int count, int boss_level){
        yield return new WaitUntil(() => GameManager.instance.playersTurn == false);

        List<(int, int)> spawnPositions = BoardManager.instance.GetSpawnPositions();
        List<GameObject> spawnEnemies = BoardManager.instance.GetSpawnEnemies(count, boss_level);

        for(int i=0; i<count; i++){
            int rand = Random.Range(0, spawnPositions.Count);
            Position spawn_pos = new Position(spawnPositions[rand].Item1, spawnPositions[rand].Item2);

            rand = Random.Range(0, spawnEnemies.Count);
            GameObject spawn_enemy = spawnEnemies[rand];

            GameManager.instance.SecretGrid[spawn_pos.x, spawn_pos.y].unit = Instantiate(spawn_enemy, new Vector2(spawn_pos.x, spawn_pos.y), Quaternion.identity);
            GameManager.instance.SecretGrid[spawn_pos.x, spawn_pos.y].unit.transform.SetParent(GameManager.instance.Enemies.transform);
            GameManager.instance.SecretGrid[spawn_pos.x, spawn_pos.y].unit.transform.GetComponent<Enemy>().takeTurn = false;
            GameManager.instance.SecretGrid[spawn_pos.x, spawn_pos.y].unit.transform.GetComponent<Enemy>().sleep_short = false;
            GameManager.instance.SecretGrid[spawn_pos.x, spawn_pos.y].unit.transform.GetComponent<Enemy>().spawned = true;

            if(i == 0) SoundManager.instance.PlaySound(spawn_sound);
            Color color = GameManager.instance.SecretGrid[spawn_pos.x, spawn_pos.y].unit.GetComponent<SpriteRenderer>().color;
            color.a = GameManager.instance.SecretGrid[spawn_pos.x, spawn_pos.y].unit.GetComponent<ColorEnemy>().color_inactive.Evaluate(0);
            GameManager.instance.SecretGrid[spawn_pos.x, spawn_pos.y].unit.GetComponent<SpriteRenderer>().color = color;
            GameManager.instance.SecretGrid[spawn_pos.x, spawn_pos.y].unit.GetComponent<Object>().SpriteFlash(1, 1, 1, 1);
            spawnPositions.Remove((spawn_pos.x, spawn_pos.y));
            if(spawnPositions.Count == 0) break;
        }

        GameManager.instance.enemy_deaths_pending -= 1;
        gameObject.SetActive(false);
        Destroy(gameObject);
    }
}
