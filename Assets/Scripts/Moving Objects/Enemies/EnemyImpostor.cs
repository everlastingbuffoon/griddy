﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyImpostor : Enemy{

    protected override void Awake(){
        base.Awake();
        takeTurn = true;
    }

    public override bool TurnCounter(){
        takeTurn = true;
        return true;
    }

    public override (int, int) MoveEnemy(){
        return (-1*PlayerManager.instance.last_move.Item1, -1*PlayerManager.instance.last_move.Item2);
    }

    public override (int, int) PanicMove(){
        return (0, 0);
    }
}