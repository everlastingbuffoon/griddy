﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructable : Object{
    public GameObject DeathParticles;
    
    public override void Die(int xDir, int yDir){
        //var shape = DeathParticles.GetComponent<ParticleSystem>().shape;
        //Instantiate(DeathParticles, transform.position, Quaternion.identity);
        GameManager.instance.scoreManager.KeepCombo();
        if(PlayerManager.instance.player_wait > 0) PlayerManager.instance.player_wait -= 1;

        // particles
        if(DeathParticles != null){
            foreach(ParticleSystem ParticleSet in DeathParticles.GetComponentsInChildren<ParticleSystem>()){
                var shape = ParticleSet.shape;
                shape.rotation = new Vector3(0, 0, (2*((xDir==-1) ? 1 : 0)+yDir)*90-(ParticleSet.shape.arc/2));
            }
        }

        GameObject particels = Instantiate(DeathParticles, transform.position, Quaternion.identity);
        particels.transform.SetParent(GameManager.instance.Particles.transform);

        gameObject.SetActive(false);
        Destroy(gameObject);
    }
}
